-- %TYPE.
CREATE TABLE departments
    (dept_id NUMBER NOT NULL PRIMARY KEY
    dept_name VARCHAR2(60));

    DECLARE
        l_num NUMBER(5,2) NOT NULL DEFAULT 2.21;
        l_num vartype l_num%TYPE := 1.123; --INHERITS DATA TYPE AND CONSTRAINT.
        l_num coltype departments.dept_id%TYPE;
BEGIN
    DBMS_OUTPUT.PUT_LINE('l_num_vartype assigned value 1.123. Final value '||l_num_vartype);
    DBMS_OUTPUT.PUT_LINE('l_num_coltype not assigned any value. Final value ' ||l_num_coltype);
END;