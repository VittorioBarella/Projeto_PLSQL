-- DATETIME | DATYPES.
    -- CURRENTLY THE TIME ZONE FOR THE DATABASE AND THE SESSION ARE BOTH PACIFIC.

SELECT current_timestamp,systimestamp from dual;

    -- SET SESSION TIME ZONE TO EASTERN
ALTER session set time_zone = 'EST';

SELECT current_timestamp,systimestamp from dual;