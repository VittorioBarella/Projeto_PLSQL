-- DEMO OF ANONYMOUS BLOCK STRUCTURE AND COMMENT STYLES.
DECLARE
    -- LOCAL PARA DECLARAR VARIÁVEIS
    l_counter NUMBER;
BEGIN
    l_counter := l; -- l_var assigned value
    dbms_output.put_line('l_counter in the inner block is ' ||l_counter);
    /* dbms_output.put_line sends output messages
       to the console
    */
EXCEPTION
    WHEN OTHERS THEN
        null;
END;