-- FOR LOOP COUNTER.

CREATE TABLE departments
(dept_id NUMBER NOT NULL PRIMARY KEY,
 dept_name VARCHAR2(60));

 INSERT INTO DEPARTMENTS (dept_id, dept_name) VALUES (1,'Sales');
 INSERT INTO DEPARTMENTS (dept_id, dept_name) VALUES (2,'IT');

DECLARE
    l_dept_count NUMBER;
BEGIN
    SELECT COUNT (*);
        INTO l_dept_count
         FROM departments;
    FOR l_counter in l..l_dept_count loop
        DBMS_OUTPUT.PUT_LINE(l_counter);
    END LOOP;
END;