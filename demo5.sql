-- PRECISION AND SCALE FOR NUMBER.
DECLARE
    -- IF YOU SPECIFY A NEGATIVE SCALE, ORACLE DATABASE ROUNDS THE ACTUAL DATA TO THE
    -- SPECIFIED NUMBER OF PLACES TO THE LEFT OF THE DECIMAL POINT.
    l_num NUMBER(5,-2):= 12345.678;
BEGIN
    DBMS_OUTPUT.PUT_LINE('l_num Assigned 12345.678 Final: '||l_num);
    l_num := 156.456;
    DBMS_OUTPUT.PUT_LINE('l_num Assigned 156.456 Final: '||l_num);
END;