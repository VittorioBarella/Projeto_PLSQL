-- BINARY_DOUBLE AND BINARY_FLOAT.
DECLARE
    l_num1 NUMBER := 0.51;
    l_num2 NUMBER ;
    l_num3 NUMBER ;
    l_bin_float BINARY_FLOAT := 2;
BEGIN
    -- EXPRESSION INVOLVING BINARY_FLOAT AND NUMBER ARE CONVERTED TO BINARY_FLOAT BASED
    -- ON DEFAULT PRECEDENCE.
    l_num2 := l_num1 * l_bin_float;
    DBMS_OUTPUT.PUT_LINE('l_num2: ' ||l_num2);

    -- STICK WITH NUMBER WITH EXPLICIT CONVERSION.
    l_num3 := l_num1 * TO_NUMBER(l_bin_float);
    DBMS_OUTPUT.PUT_LINE('l_num3: ' ||l_num3);

    -- BINARY FLOAT COMPUTATIONS SHOULD BE CHECKED WITH PRE-DEFINED CONSTANTS FOR ERRORS.
    l_bin_float := l_bin_float / 0; -- INFINITY
    IF l_bin_float = BINARY_FLOAT_INFINITY THEN
        DBMS_OUTPUT.PUT_LINE('l_bin_float IS: '||l_bin_float);
    END IF;
END;
