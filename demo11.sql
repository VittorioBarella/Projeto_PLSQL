-- DATETIME | DATYPES.

DECLARE
    l_date DATE := current_date;
    l_systimestamp timestamp with time zone := systimestamp;
    l_currenttimestamp timestamp with time zone := current_timestamp;
    l_timestamp timestamp := current_timestamp;

BEGIN
DBMS_OUTPUT.PUT_LINE('l_date ' ||l_date);
-- SYSTEM OR DB TIMESTAMP IN PACIFIC ZONE WITH -8 OFFSET.

DBMS_OUTPUT.PUT_LINE('l_systimestamp ' ||l_systimestamp);
-- CURRENT TIMESTAMP SHOWS THE SESSION TIME IN EASTERN ZONE.

DBMS_OUTPUT.PUT_LINE('l_currenttimestamp ' ||l_currenttimestamp);
-- CURRENT TIMESTAMP FETCHED IN TIMESTAMP VARIABLE LOOSING THE TIMEZONE INFORMATION.

DBMS_OUTPUT.PUT_LINE('l_timestamp ' ||l_timestamp);
END;
