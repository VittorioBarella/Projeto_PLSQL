-- REVERSE FOR LOOP.
BEGIN
    FOR l_counter in REVERSE 1..3 LOOP
        DBMS_OUTPUT.PUT_LINE(l_counter);
    END LOOP
END;